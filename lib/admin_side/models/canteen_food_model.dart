class CanteenFood{
  String foodName;
  String foodImage;
  int foodPrize;
  bool isAvailable;



  CanteenFood({
    required this.foodName,
    required this.foodImage,
    required this.foodPrize,
    required this.isAvailable
  });

}