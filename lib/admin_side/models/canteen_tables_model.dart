class CanteenTables{

  String tableImage;
  String tableName;
  int tableNumber;
  int tableCapacity;
  bool isAvailable;

  CanteenTables({
    required this.tableImage,
    required this.tableName,
    required this.tableNumber,
    required this.tableCapacity,
    required this.isAvailable
});



}