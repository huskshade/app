import 'package:flutter/material.dart';

class CanteenInfo {
  String canteenName;
  String address;
  String email;
  String imagePath;

  CanteenInfo({
    required this.canteenName,
    required this.address,
    required this.email,
    required this.imagePath,
  });
}
