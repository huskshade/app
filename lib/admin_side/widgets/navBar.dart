// import 'package:application_savour/admin_side/admin_profile_page.dart';
// import 'package:application_savour/admin_side/home_page.dart';
// import 'package:application_savour/admin_side/table_reservation_tab/reservation_manage_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

import '../admin_profile_page.dart';
import '../home_page.dart';
import '../table_reservation_tab/reservation_manage_page.dart';

class NavBar extends StatefulWidget {
  const NavBar({super.key});

  @override
  State<NavBar> createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  int selectedIndex = 0;
  final screens = [
    HomePage(),
    ReservationPage(),
    AdminProfile(),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: screens[selectedIndex],
        bottomNavigationBar: Container(
          color: Colors.deepOrangeAccent,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 10),
            child: GNav(
              onTabChange: (value) {
                setState(() {
                  selectedIndex = value;
                });
              },
              backgroundColor: Colors.deepOrangeAccent,
              color: Colors.white,
              activeColor: Colors.brown,
              tabBackgroundColor: Colors.yellow.shade200,
              padding: const EdgeInsets.all(14),
              gap: 8,
              tabs: const [
                GButton(
                  icon: Icons.home,
                  text: 'Home',
                ),
                GButton(
                  icon: Icons.table_bar_rounded,
                  text: 'Reservations',
                ),
                GButton(
                  icon: Icons.person_2_rounded,
                  text: 'Profile',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
