// import 'package:application_savour/create_foods/display_food.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Foods extends StatefulWidget {
  const Foods({super.key});

  @override
  State<Foods> createState() => _FoodsState();
}

class _FoodsState extends State<Foods> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
        body: Center(
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: SizedBox(
              height: 600,
              width: 350,
              child: Card(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(

                      children: [
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                          color: Colors.grey,
                          height: 200,
                            width: 300,
                            child: const Text('Food Image', style: TextStyle(fontSize: 20),)),
                         Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                              const Text('Food Name', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                            const SizedBox(height: 10,),
                            const TextField(
                              decoration: InputDecoration(
                                hintText: 'Enter text here', // Add your hint text
                                border: OutlineInputBorder(  // Add a border around the text field
                                  borderSide: BorderSide(
                                    color: Colors.black, // Set the border color
                                    width: 2.0, // Set the border width
                                  ),
                                ),
                              ),
                            ),
                           const SizedBox(height: 10,),
                           const Text('Price', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                          const  SizedBox(height: 10,),
                            const TextField(
                              decoration: InputDecoration(
                                hintText: 'Enter text here', // Add your hint text
                                border: OutlineInputBorder(  // Add a border around the text field
                                  borderSide: BorderSide(
                                    color: Colors.black, // Set the border color
                                    width: 2.0, // Set the border width
                                  ),
                                ),
                              ),
                            ),
                              const SizedBox(height: 30,),
                              Center(
                                child: ElevatedButton(
                                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.orangeAccent),
                                    minimumSize: MaterialStateProperty.all<Size>(
                                     const Size(200, 60), // Set the width and height you desire
                                    ),),
                                  onPressed: (){

                                },
                                  child:const Text('Save'),
                                ),
                              )
                          ],
                        )


                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
    ));
  }
}
