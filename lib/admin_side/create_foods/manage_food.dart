// import 'package:application_savour/create_foods/display_food.dart';
// import 'package:application_savour/admin_side/create_foods/foods.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'display_food.dart';
import 'foods.dart';

class FoodManage extends StatefulWidget {
  const FoodManage({super.key});

  @override
  State<FoodManage> createState() => _FoodManageState();
}

class _FoodManageState extends State<FoodManage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
        appBar: AppBar( // Add this AppB
          backgroundColor: Colors.orange,// ar widget
          title: Text('Foods', style: TextStyle(fontWeight: FontWeight.w800, color: Colors.white),), // Set the title of the AppBar
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            const Padding(
              padding: EdgeInsets.fromLTRB(10,20,0,20),
              child: Text('Manage Foods',
                style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),),
            ),

            Center(
                child: ElevatedButton(style: ElevatedButton.styleFrom(

                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  backgroundColor: Colors.brown.shade500, // Change the background color here
                ),

                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const Foods()),
                      );
                    },
                    child: const Text("+ Add Foods", style: TextStyle(fontWeight: FontWeight.w400, color: Colors.orangeAccent),))),
                     const SizedBox(height: 20,),
                Expanded(
              child: ListView.builder(
                itemCount: 5,
                  itemBuilder: (BuildContext context, int index){
                    return DisplayFood();
              }),
            )



          ],
        )
    ));
  }
}


