import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class DisplayFood extends StatefulWidget {
  const DisplayFood({
    super.key,
  });

  @override
  State<DisplayFood> createState() => _DisplayFoodState();
}

class _DisplayFoodState extends State<DisplayFood> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding:const EdgeInsets.all(20.0),
        child: SizedBox(
          height: 470,
          width: 350,
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(

                children: [
                  Container(
                      margin: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                      color: Colors.grey,
                      height: 200,
                      width: 300,
                      child: const Text('Food Image', style: TextStyle(fontSize: 20),)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      const Text('Food Name: ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                      const SizedBox(height: 10,),

                      const SizedBox(height: 10,),
                      const Text('Price: ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                      const  SizedBox(height: 10,),

                      const SizedBox(height: 20,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [

                          ElevatedButton(
                            style: ButtonStyle(fixedSize: MaterialStateProperty.all<Size>(
                              const Size(160, 60),
                              // Set the width and height you desire
                            ),shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              const RoundedRectangleBorder(
                                borderRadius: BorderRadius.zero, // Set the border radius to zero
                              ),
                            ),

                            ),
                            onPressed: (){

                            },
                            child:const Icon(Icons.edit) ,
                          ),
                          ElevatedButton(
                            style: ButtonStyle(fixedSize: MaterialStateProperty.all<Size>(
                              const Size(160, 60),
                              // Set the width and height you desire
                            ),shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              const RoundedRectangleBorder(
                                borderRadius: BorderRadius.zero, // Set the border radius to zero
                              ),
                            ), backgroundColor: MaterialStateProperty.all<Color>(Colors.red.shade400),),
                            onPressed: (){

                            },
                            child:const Icon(Icons.delete,color: Colors.white,) ,
                          ),

                        ],
                      ),
                      const SizedBox(height: 30,),



                    ],
                  )


                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
