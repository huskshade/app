import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';
import 'dart:io';

import '../login_page.dart';
import 'models/canteen_info_model.dart';

class AdminProfile extends StatefulWidget {
  const AdminProfile({super.key});
  
  

  @override
  State<AdminProfile> createState() => _AdminProfileState();
}



class _AdminProfileState extends State<AdminProfile> {

  CanteenInfo canteenInfo = CanteenInfo(
    canteenName: '',
    address: '',
    email: '',
    imagePath: '',
  );

  //get image from gallery
  Future<void> pickImage()async{
     final pickedImage = await ImagePicker().pickImage(source: ImageSource.gallery);
     if(pickedImage != null){
       setState(() {
         canteenInfo.imagePath = pickedImage.path;
       });
     }
  }

TextEditingController canteenNameController = TextEditingController();
TextEditingController addressController = TextEditingController();
TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title:const  Text('Profile', style: TextStyle(color: Colors.white),),
          automaticallyImplyLeading: false,
          centerTitle: true,
          backgroundColor: Colors.brown,
          actions: [

        PopupMenuButton<String>(
          icon: const Icon(Icons.settings),
          color: Colors.white,
          onSelected: (String value) {
            if (value == 'Log out') {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => LoginPage(), // Replace 'LoginPage()' with your actual login page.
              ));
            }
          },
      itemBuilder: (BuildContext context) => [

       const PopupMenuItem<String>(
          value: 'Log out',
          child: Text('Log Out'),
        ),
          ],
        ),
          ],
        ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [


                 GestureDetector(
                   onTap: (){
                     pickImage();
                     print("tapped");
                   },
                   child: Container(
                     height: 250,
                     width: double.infinity,
                     color: Colors.grey,
                     child: canteenInfo.imagePath.isNotEmpty
                         ? Image.file(File(canteenInfo.imagePath))
                         : Icon(Icons.add_a_photo_sharp, size: 100),
                   ),

                 ),

                  const Text('Canteen Name', style: TextStyle(
                    fontWeight: FontWeight.w800, fontSize: 30,
                  ),),

                  Padding(

                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                     decoration: InputDecoration(
                       labelText: 'Name',
                       border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10), // Adjust the border radius
                       ),
                     ),
                      controller: canteenNameController,
                      onChanged: (value){
                       setState(() {
                         canteenInfo.canteenName = value;
                       });
                      },
                    ),
                  ),
                  const Text('Canteen Address', style: TextStyle(
                    fontWeight: FontWeight.w800, fontSize: 30,
                  ),),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      decoration: InputDecoration(
                        labelText: 'Address',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10), // Adjust the border radius
                        ),
                      ),
                      controller: addressController,
                      onChanged: (value){
                      setState(() {
                      canteenInfo.address = value;
                      });
                      },

                    ),

                  ),
                  const Text('Email', style: TextStyle(
                    fontWeight: FontWeight.w800, fontSize: 30,
                  ),),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      decoration: InputDecoration(
                        labelText: 'email@gmail.com',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10), // Adjust the border radius
                        ),
                      ),
                      controller: emailController,
                      onChanged: (value){
                        setState(() {
                          canteenInfo.email = value;
                        });
                      },
                    ),
                  ),
                  Center(
                    child: SizedBox(
                      height: 60,
                      width: 150,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: CupertinoColors.activeGreen,
                          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10)
                        ),
                        onPressed: (){
                            print(canteenInfo.canteenName);
                            print(canteenInfo.address);
                            print(canteenInfo.email);

                      },
                          child:  const Text('Save',style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.w700),),
                          ),
                    ),
                  )
                ],
              ),
            ),
          ),
      )
        );



  }

  void handleMenuItemSelected(BuildContext context, String value) {
    if (value == 'Log out') {
      // Handle the Settings option
      print('logout Function');
    }
  }
}
