// import 'package:application_savour/create_tables/display_table.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

import 'package:savour/admin_side/models/canteen_tables_model.dart';

class Tables extends StatefulWidget {
  const Tables({super.key});

  @override
  State<Tables> createState() => _TablesState();
}

class _TablesState extends State<Tables> {

  CanteenTables canteenTables = CanteenTables(
      tableImage: '',
      tableName: '',
      tableNumber: 0,
      tableCapacity: 0,
      isAvailable: false
    );

  List<CanteenTables> canteenTablesList = [];


  //get image from gallery
  Future<void> pickImage()async{
    final pickedImage = await ImagePicker().pickImage(source: ImageSource.gallery);
    if(pickedImage != null){
      setState(() {
        canteenTables.tableImage = pickedImage.path;
      });
    }
  }
  void updateTableName(String value) {
    setState(() {
      canteenTables.tableName = value;
    });
  }
  void updateTableNumber(String value) {
    setState(() {
      canteenTables.tableNumber = int.parse(value);
    });
  }
  void updateTableCapacity(String value) {
    setState(() {
      canteenTables.tableCapacity = int.parse(value);
    });
  }
  TextEditingController tableNumberController = TextEditingController();
  TextEditingController tableNameController = TextEditingController();
  TextEditingController tableCapacityController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(

          body: Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(

                          width: 350,
                          child:Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                               GestureDetector(

                                    onTap: pickImage,
                                    child: Container(
                            height: 250,
                            width: double.infinity,
                            color: Colors.grey,
                            child: canteenTables.tableImage.isNotEmpty
                                ? Image.file(File(canteenTables.tableImage))
                                : const Icon(Icons.add_a_photo_sharp, size: 100),
                          ),
                                  ),

                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text("Add Table", style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),),
                              ),

                              const Padding(
                                padding:  EdgeInsets.fromLTRB(8.0,8.0,8.0,0),
                                child: Text("Table Name", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),),
                              ),
                              Padding(
                                padding:const EdgeInsets.all(8.0),
                                child: SizedBox(
                                  width: double.infinity,
                                  height: 50,
                                  child: TextField(
                                    keyboardType: TextInputType.name,
                                    style: const TextStyle(fontSize: 20),
                                    decoration: const InputDecoration(
                                      labelText: 'Name',
                                      border: OutlineInputBorder(),
                                    ),
                                    controller: tableNameController,
                                    onChanged: updateTableName,
                                  ),
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text("Table number", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),),
                              ),
                               Padding(
                                padding:const EdgeInsets.fromLTRB(8.0,8.0,8.0,0),
                                child: SizedBox(
                                  width: double.infinity,
                                  height: 50,
                                  child: TextField(
                                          keyboardType:TextInputType.number,
                                    style: const TextStyle(fontSize: 20),
                                    decoration:const InputDecoration(
                                      labelText: 'Table number',
                                      border: OutlineInputBorder(),
                                    ),
                                   controller: tableNumberController,
                                    onChanged: updateTableNumber,
                                  ),
                                ),
                              ),
                              const Padding(
                                padding:  EdgeInsets.fromLTRB(8.0,8.0,8.0,0),
                                child: Text("Table capacity", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),),
                              ),
                              Padding(
                                padding:const EdgeInsets.all(8.0),
                                child: SizedBox(
                                  width: double.infinity,
                                  height: 50,
                                  child: TextField(
                                    keyboardType: TextInputType.number,
                                    style: const TextStyle(fontSize: 20),
                                    decoration:const InputDecoration(
                                      labelText: 'seat capacity',
                                      border: OutlineInputBorder(),
                                    ),
                                    controller: tableCapacityController,
                                    onChanged: updateTableCapacity,
                                  ),
                                ),
                              ),
                              const Padding(
                                padding:  EdgeInsets.fromLTRB(8.0,8.0,8.0,0),
                                child: Text("Table Status", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),),
                              ),
                              Row(
                                children: [
                                  Switch(
                                    value: canteenTables.isAvailable,
                                    onChanged: (value) {
                                      setState(() {
                                        canteenTables.isAvailable = value;
                                      });
                                    },
                                  ),
                                  Text(
                                    canteenTables.isAvailable ? "Available" : "Unavailable",
                                    style:const TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),


                  const SizedBox(height: 10,),
                  SizedBox(
                    width: 200,
                    height: 50,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Colors.orangeAccent),
                      onPressed: () {
                       Navigator.pop(context);
                      },
                      child:const Text('Save'),
                    ),
                  )

                ],

              ),
            ),

          ),

    ));
  }
}
