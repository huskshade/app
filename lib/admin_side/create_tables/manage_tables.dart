// import 'package:application_savour/create_tables/display_table.dart';
// import 'package:application_savour/admin_side/create_tables/tables.dart';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:savour/admin_side/create_tables/tables.dart';

import '../models/canteen_tables_model.dart';
// import 'display_table.dart';

class TableManage extends StatefulWidget {
  const TableManage({super.key});

  @override
  State<TableManage> createState() => _TableManageState();
}

class _TableManageState extends State<TableManage> {
  final List<CanteenTables> tablesList = [];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.orange,
            title: const Text('Tables',
              style: TextStyle(fontWeight: FontWeight.w800, color: Colors.white),),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

           const Padding(
            padding: EdgeInsets.fromLTRB(10,10,0,10),
            child: Text('Manage Tables',
            style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),),
          ),

              Center(
                  child: ElevatedButton(style: ElevatedButton.styleFrom(

                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                   backgroundColor: Colors.brown.shade500, // Change the background color here
                  ),

                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const Tables()),
                        );
                      },
                      child: const Text("+ Create table", style: TextStyle(fontWeight: FontWeight.w400, color: Colors.orangeAccent),))),

                    const SizedBox(height: 20,),
               Expanded(
                 child: ListView.builder(
                    itemCount: tablesList.length,
                      itemBuilder: (BuildContext context, int index){
                    return Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: SizedBox(
                        height: 400,
                        width: double.infinity,
                        child: Card(
                          elevation: 5,
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.only(bottom: 20),
                                      height: 200,
                                      width: double.infinity,
                                      color: Colors.grey,
                                      child: tablesList[index].tableImage.isNotEmpty
                                          ? Image.file(File(tablesList[index].tableImage))
                                          : const Icon(Icons.add_a_photo_sharp, size: 100),
                                    ),
                                    Text(
                                      "Table Name: ${tablesList[index].tableName} ",
                                      style:const TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        Text(
                                          "Table Number: ${tablesList[index].tableNumber}",
                                          style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
                                        ),
                                        Text(
                                          "Table Status: ${tablesList[index].isAvailable ? "Available": "Unavailable"}",
                                          style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        ElevatedButton(
                                          style: ButtonStyle(
                                            fixedSize: MaterialStateProperty.all<Size>(
                                              const Size(160, 60),
                                              // Set the width and height you desire
                                            ),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                              const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.zero, // Set the border radius to zero
                                              ),
                                            ),
                                          ),
                                          onPressed: () {
                                            //edit function
                                          },
                                          child: const Icon(Icons.edit),
                                        ),
                                        ElevatedButton(
                                          style: ButtonStyle(
                                            fixedSize: MaterialStateProperty.all<Size>(
                                              const Size(160, 60),
                                              // Set the width and height you desire
                                            ),
                                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                              const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.zero, // Set the border radius to zero
                                              ),
                                            ),
                                            backgroundColor: MaterialStateProperty.all<Color>(Colors.red.shade400),
                                          ),
                                          onPressed: () {
                                            //delete function
                                          },
                                          child: const Icon(Icons.delete, color: Colors.white),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
               ),

            ],
      )),
    );
  }
}
