//import 'package:application_savour/admin_side/canteen_table_view.dart';
// import 'package:application_savour/admin_side/models/canteen_info_model.dart';
// import 'package:application_savour/admin_side/create_foods/manage_food.dart';
// import 'package:application_savour/admin_side/create_tables/manage_tables.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'create_foods/manage_food.dart';
import 'create_tables/manage_tables.dart';
import 'models/canteen_info_model.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<CanteenInfo> listOfCanteen = List.empty(growable: true);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.brown,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: const Text('Home',style: TextStyle(color: Colors.white),),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.fromLTRB(20, 25, 0, 0),
              child: Text(
                "Canteen",
                style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
              ),
            ),
            Center(
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => TableManage(),
                    ),
                  );
                },
                child: Container(
                  margin: const EdgeInsets.only(top: 20),
                  width: 350,
                  height: 250,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          spreadRadius: 1,
                          blurRadius: 10,
                          offset: const Offset(0, 2),
                        )
                      ]),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 25),
                        height: 200,
                        width: 300,
                        child: Image.network(
                          'https://www.tavolisedie.com/modules/ph_simpleblog/featured/77.jpg',
                          loadingBuilder: (BuildContext context, Widget child,
                              ImageChunkEvent? loadingProgress) {
                            if (loadingProgress == null) {
                              return child; // Image is fully loaded, so display it.
                            } else {
                              return Center(
                                child: CircularProgressIndicator(
                                  value: loadingProgress.expectedTotalBytes !=
                                          null
                                      ? loadingProgress.cumulativeBytesLoaded /
                                          loadingProgress.expectedTotalBytes!
                                      : null,
                                ),
                              );
                            }
                          },
                        ),
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Icon(
                            Icons.add,
                            size: 25,
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(5, 0, 30, 0),
                            child: Text(
                              'Create Tables',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(20, 25, 0, 0),
              child: Text(
                "Food",
                style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
              ),
            ),
            Center(
              child: Container(
                margin: const EdgeInsets.only(top: 20),
                width: 350,
                height: 310,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        spreadRadius: 1,
                        blurRadius: 10,
                        offset: const Offset(0, 2),
                      )
                    ]),
                child: GestureDetector(
                  onTap: () {
              Navigator.of(context).push(
              MaterialPageRoute(
              builder: (context) => FoodManage(),
              ),
              );
              },
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 25),
                        height: 260,
                        width: 300,
                        child: Image.network(
                          'https://as1.ftcdn.net/v2/jpg/02/50/86/70/1000_F_250867086_IASUSbFRowP5HkD7fNvlFOauHmZh6nIE.jpg',
                          loadingBuilder: (BuildContext context, Widget child,
                              ImageChunkEvent? loadingProgress) {
                            if (loadingProgress == null) {
                              return child; // Image is fully loaded, so display it.
                            } else {
                              return Center(
                                child: CircularProgressIndicator(
                                  value: loadingProgress.expectedTotalBytes !=
                                          null
                                      ? loadingProgress.cumulativeBytesLoaded /
                                          loadingProgress.expectedTotalBytes!
                                      : null,
                                ),
                              );
                            }
                          },
                        ),
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Icon(
                            Icons.add,
                            size: 25,
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(5, 0, 30, 0),
                            child: Text(
                              'Add Menu ',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 50,
            )
          ],
        ),

        // child: Center(
        //   child: Column(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: [
        //
        //       const SizedBox(height: 20,),
        //       ElevatedButton(onPressed: () {
        //         Navigator.push(
        //           context,
        //           MaterialPageRoute(
        //               builder: (context) => const CanteenInfoCreation()),
        //         );
        //       }
        //           , child: const Text("Add Canteen")
        //       ),
        //       //  ListView.builder(itemCount: listOfCanteen.length,
        //       //   itemBuilder: (context, index) => getData(index),
        //       // )
        //
        //     ],
        //   ),
        // ),
      ),
    ));
  }

  // Widget getData(int index) {
  //  return SizedBox(
  //    height: 250,
  //    width: 200,
  //    child: Text("anjadanddda"),
  //  );
  // }
}
