// import 'package:application_savour/admin_side/table_reservation_tab/reservation_food_tab.dart';
// import 'package:application_savour/admin_side/table_reservation_tab/reservation_history.dart';
// import 'package:application_savour/admin_side/table_reservation_tab/reservation_table_tab.dart';
//import 'package:application_savour/table_reservation_tab/reservation_table_tab.dart';
import 'package:flutter/material.dart';
import 'package:savour/admin_side/table_reservation_tab/reservation_food_tab.dart';
import 'package:savour/admin_side/table_reservation_tab/reservation_history.dart';
import 'package:savour/admin_side/table_reservation_tab/reservation_table_tab.dart';

class ReservationPage extends StatefulWidget {
  const ReservationPage({super.key});

  @override
  State<ReservationPage> createState() => _ReservationPageState();
}

class _ReservationPageState extends State<ReservationPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.brown,
            automaticallyImplyLeading: false,
            centerTitle: true,
            title: const Text('Reservations',style: TextStyle(color: Colors.white),),
          ),
          body: Column(
            children: [
              TabBar(tabs: [
                Tab(
                  icon: Icon(Icons.table_restaurant),

                ),
                Tab(
                  icon: Icon(Icons.fastfood),

                ),
                Tab(
                  icon: Icon(Icons.manage_history),

                )
              ]
              ),
              Expanded(
                child: TabBarView(children: [
                ReservationTableTab(),
                  ReservationFoodTab(),
                  ReservationHistoryTab(),
                ]),
              )
            ],
          ),

      ),
      ),
    );
  }
}
