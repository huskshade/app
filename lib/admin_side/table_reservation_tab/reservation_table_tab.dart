
import 'package:flutter/material.dart';

class ReservationTableTab extends StatefulWidget {
  const ReservationTableTab({super.key});

  @override
  State<ReservationTableTab> createState() => _ReservationTableTabState();
}

class _ReservationTableTabState extends State<ReservationTableTab> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          const Padding(
            padding: EdgeInsets.fromLTRB(8.0,20,0,0),
            child: Text('Table Reservations', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
          ),
             Expanded(
                      child:
                        ListView.builder(
            itemCount: 5,
            itemBuilder: (BuildContext context, int index) {
              return const DisplayReservationTable();
            },
          )),
        ],
      ),
    ));
  }
}

class DisplayReservationTable extends StatefulWidget {
  const DisplayReservationTable({
    super.key,
  });

  @override
  State<DisplayReservationTable> createState() => _DisplayReservationTableState();
}

class _DisplayReservationTableState extends State<DisplayReservationTable> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 3,
        child: ListTile(
          leading: const CircleAvatar(

          ),
        title: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Column(

            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('name@gmail.com'),
              Text('09971277666'),
              Text('Reservation for Table: 1', style: TextStyle(fontWeight: FontWeight.bold),),

              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(Colors.greenAccent), // Set the background color
                      ),
                      onPressed: (){

                  },
                      child:const Text('Accept')),
                  const SizedBox(width: 15,),
                  ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(Colors.red), // Set the background color
                      ),onPressed: (){

                  },
                      child: Text('Deny', style: TextStyle(color: Colors.white),))
                ],
              )



            ],
          ),
        ),
        ),

      ),
    );
  }
}

