import 'package:flutter/material.dart';

class ReservationFoodTab extends StatefulWidget {
  const ReservationFoodTab({super.key});

  @override
  State<ReservationFoodTab> createState() => _ReservationFoodTabState();
}

class _ReservationFoodTabState extends State<ReservationFoodTab> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.fromLTRB(8.0,20,0,0),
              child: Text('Table Reservations', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: 5,
                itemBuilder: (BuildContext context, int index) {
                  return const DisplayReservationFood();
                },
              ),
            ),
          ],
        )
    ),
    );
  }

  }

class DisplayReservationFood extends StatefulWidget {
  const DisplayReservationFood({
    super.key,
  });

  @override
  State<DisplayReservationFood> createState() => _DisplayReservationFoodState();
}

class _DisplayReservationFoodState extends State<DisplayReservationFood> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 3,
        child: ListTile(
          leading: const CircleAvatar(

          ),
          title: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('name@gmail.com'),
                const Text('09971277666'),
                const Text('Reservation for Food: Sinigang', style: TextStyle(fontWeight: FontWeight.bold),),


                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.greenAccent), // Set the background color
                        ),
                        onPressed: (){

                        },
                        child:const Text('Accept')),
                    const SizedBox(width: 15,),
                    ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.red), // Set the background color
                        ),onPressed: (){

                    },
                        child: Text('Deny', style: TextStyle(color: Colors.white),))
                  ],
                )



              ],
            ),
          ),
        ),

      ),
    );
  }
}

