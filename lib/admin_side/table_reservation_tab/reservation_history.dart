import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ReservationHistoryTab extends StatefulWidget {
  const ReservationHistoryTab({super.key});

  @override
  State<ReservationHistoryTab> createState() => _ReservationHistoryTabState();
}

class _ReservationHistoryTabState extends State<ReservationHistoryTab> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body: Column(
        children: [
          const Padding(
            padding: EdgeInsets.fromLTRB(8.0,20,0,0),
            child: Text('History', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
          ),
        ],
      ),
    ));
  }
}
