import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:savour/admin_side/home_page.dart';
import 'package:savour/admin_side/widgets/navBar.dart';
import '../../constant/utils.dart';

Future<void> userLogin(BuildContext context, String username, String pass) async {
  final Uri url = Uri.parse('${Utils.baseUrl}/user/login');

  try {
    final response = await http.post(
      url,
      headers: {"accept": "application/json"},
      body: {'username': username, 'password': pass},
    );

    if (response.statusCode == 200) {
      var decodeData = jsonDecode(response.body);
      print(decodeData);
      _showSuccessSnackBar;

      // Check if login was successful (you can define your own condition)
      if (decodeData['success']) {
        // Navigate to the homepage if login is successful

        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context) => NavBar(),

          ),
        );
      } else {
        // Handle unsuccessful login (e.g., show an error message)
        print('Login failed: ${decodeData['message']}');
      }
    }
    if(response.statusCode == 401){
      showErrorDialog(context, "Wrong Email or Password....");
    }
    else {
      // Handle non-200 status codes
      print("Login failed with status code: ${response.statusCode}");
    }
  } catch (e) {
    // Handle other exceptions (e.g., network errors)
    print("Error during login: $e");
    throw Exception('Failed to login');
  }

}



Future<void> userRegister(BuildContext context, String username, String password, String contact, String address) async {
  final Uri url = Uri.parse('${Utils.baseUrl}/user/register');

  try {
    final response = await http.post(
      url,
      headers: {"Content-Type": "application/json"},
      body: json.encode({
        "username": username,
        "password": password,
        "contact": contact,
        "address": address,
      }),
    );

    if (response.statusCode == 400) {
      // Username already exists, handle the error message
      showErrorDialog(context, "username exist");
      final responseData = json.decode(response.body);
      print(responseData['message']);
    }
  if (response.statusCode == 200) {
      // Registration was successful
      final responseData = json.decode(response.body);
      print(responseData['message']);
      _showSuccessSnackBar(context);

      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => NavBar(),

        ),
      );
    } else {
      // Handle other error cases here
      print("Failed to register. Status code: ${response.statusCode}");
    }
  } catch (error) {
    // Handle network or other errors here
    print("Error during registration: $error");
  }
}


void showErrorDialog(BuildContext context, String errorMessage) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: AlertDialog(

          content: Text(
            errorMessage,
            style:const TextStyle(
              fontSize: 18,
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child:const Text(
                "OKAY",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.blue, // Customize the button color
                ),
              ),
            ),
          ],
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 5,
          backgroundColor: Colors.white, // Customize the background color
        ),
      );
    },
  );
}

void _showSuccessSnackBar(BuildContext context) {
  const snackBar = SnackBar(content: Text("Success"));
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}