import 'dart:convert';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:savour/admin_side/home_page.dart';
import 'admin_side/rest/rest_api.dart';
import 'login_page.dart';
import 'package:http/http.dart' as http;

import 'package:fluttertoast/fluttertoast.dart';

class Register extends StatefulWidget {
  Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  TextEditingController username = TextEditingController();

  TextEditingController pass = TextEditingController();

  TextEditingController contactNumber = TextEditingController();

  TextEditingController address = TextEditingController();

  final formKey = GlobalKey<FormState>();

  // Future register(BuildContext context) async {
  //   var url = Uri.parse("http://192.168.0.18/savour_api/sign_in.php");
  //   var response = await http.post(url, body: {
  //     'username': username.text,
  //     'password': pass.text,
  //     'address': address.text,
  //     'contact': contactNumber.toString(),
  //   });
  //   var data = json.decode(response.body);
  //   if (data == "Error") {
  //     Fluttertoast.showToast(
  //       backgroundColor: Colors.orange,
  //       textColor: Colors.white,
  //       msg: 'User already exit!',
  //       toastLength: Toast.LENGTH_SHORT,
  //     );
  //   } else {
  //     Fluttertoast.showToast(
  //       backgroundColor: Colors.green,
  //       textColor: Colors.white,
  //       msg: 'Registration Successful',
  //       toastLength: Toast.LENGTH_SHORT,
  //     );
  //     Navigator.push(
  //       context,
  //       MaterialPageRoute(
  //         builder: (context) => HomePage(),
  //       ),
  //     );
  //   }
  // }

  bool isPasswordVisible = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.bottomRight,
                  end: Alignment.topRight,
                  colors: [
                    Color.fromARGB(255, 80, 198, 241),
                    Color.fromARGB(255, 223, 141, 58)
                  ])),
          child: SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  const SizedBox(height: 20,),
                  const CircleAvatar(
                    backgroundImage: AssetImage('lib/logo/logo.png'),
                    radius: 90,
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child: TextFormField(
                      controller: username,
                      style: const TextStyle(color: Colors.white, fontSize: 14.5),
                      decoration: InputDecoration(
                        prefixIconConstraints: const BoxConstraints(minWidth: 45),
                        prefixIcon: const Icon(
                          Icons.alternate_email_outlined,
                          color: Colors.white70,
                          size: 22,
                        ),
                        border: InputBorder.none,
                        hintText: 'Enter Email',
                        hintStyle: const TextStyle(
                          color: Colors.white60,
                          fontSize: 14.5,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(100).copyWith(
                            bottomRight: const Radius.circular(0),
                          ),
                          borderSide: const BorderSide(
                            color: Colors.white38,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(100).copyWith(
                            bottomRight: const Radius.circular(0),
                          ),
                          borderSide: const BorderSide(
                            color: Colors.white70,
                          ),
                        ),
                        errorBorder: OutlineInputBorder( // Add this to set the border when there's an error
                          borderRadius: BorderRadius.circular(100).copyWith(
                            bottomRight: const Radius.circular(0),
                          ),
                          borderSide: const BorderSide(
                            color: Colors.red, // Customize the error border color as needed
                          ),
                        ),
                        errorStyle: TextStyle(color: Colors.red), // Customize error text style
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Email is required';
                        } else if (!RegExp(r'^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$').hasMatch(value!)) {
                          return 'Enter a valid email address';
                        }
                        return null; // Return null for no validation errors
                      },
                    ),
                  ),

                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child: TextField(
                      controller: pass,
                      style: const TextStyle(color: Colors.white, fontSize: 14.5),
                      obscureText: isPasswordVisible ? false : true,
                      decoration: InputDecoration(
                          prefixIconConstraints:
                          const BoxConstraints(minWidth: 45),
                          prefixIcon: const Icon(
                            Icons.lock,
                            color: Colors.white70,
                            size: 22,
                          ),
                          suffixIconConstraints:
                          const BoxConstraints(minWidth: 45, maxWidth: 46),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                isPasswordVisible = !isPasswordVisible;
                              });
                            },
                            child: Icon(

                              isPasswordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.white70,
                              size: 22,
                            ),
                          ),
                          border: InputBorder.none,
                          hintText: 'Enter Password',
                          hintStyle: const TextStyle(
                              color: Colors.white60, fontSize: 14.5),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100).copyWith(
                                  bottomRight: const Radius.circular(0)),
                              borderSide:
                              const BorderSide(color: Colors.white38)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100).copyWith(
                                  bottomRight: const Radius.circular(0)),
                              borderSide:
                              const BorderSide(color: Colors.white70))),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child: TextField(
                      keyboardType: TextInputType.text,
                      controller: address,
                      style: const TextStyle(color: Colors.white, fontSize: 14.5),
                      decoration: InputDecoration(
                          prefixIconConstraints:
                          const BoxConstraints(minWidth: 45),
                          prefixIcon: const Icon(
                            Icons.location_city,
                            color: Colors.white70,
                            size: 22,
                          ),
                          border: InputBorder.none,
                          hintText: 'Adress',
                          hintStyle: const TextStyle(
                              color: Colors.white60, fontSize: 14.5),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100).copyWith(
                                  bottomRight: const Radius.circular(0)),
                              borderSide:
                              const BorderSide(color: Colors.white38)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100).copyWith(
                                  bottomRight: const Radius.circular(0)),
                              borderSide:
                              const BorderSide(color: Colors.white70))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child:TextFormField(
                      keyboardType: TextInputType.number,
                      controller: contactNumber,
                      style: const TextStyle(color: Colors.white, fontSize: 14.5),
                      decoration: InputDecoration(
                        prefixIconConstraints: const BoxConstraints(minWidth: 45),
                        prefixIcon: const Icon(
                          Icons.call,
                          color: Colors.white70,
                          size: 22,
                        ),
                        hintText: 'Contact number',
                        hintStyle: const TextStyle(
                          color: Colors.white60,
                          fontSize: 14.5,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(100).copyWith(
                            bottomRight: const Radius.circular(0),
                          ),
                          borderSide: const BorderSide(
                            color: Colors.white38,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(100).copyWith(
                            bottomRight: const Radius.circular(0),
                          ),
                          borderSide: const BorderSide(
                            color: Colors.white70,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(100).copyWith(
                            bottomRight: const Radius.circular(0),
                          ),
                          borderSide: const BorderSide(
                            color: Colors.red, // You can customize the error border color here
                          ),
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(100).copyWith(
                            bottomRight: const Radius.circular(0),
                          ),
                          borderSide: const BorderSide(
                            color: Colors.red, // You can customize the focused error border color here
                          ),
                        ),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Contact number is required';
                        } else if (value.length != 11 || !RegExp(r'^[0-9]+$').hasMatch(value)) {
                          return 'Contact number must be exactly 11 digits';
                        }
                        return null; // Return null for no validation errors
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  TextButton(
                    onPressed: () {
                      // Call the registration function here
                     // register(context);
            if (formKey.currentState!.validate()) {

       username.text.isNotEmpty && pass.text.isNotEmpty && contactNumber.text.isNotEmpty
            && address.text.isNotEmpty?
        userRegister(context, username.text, pass.text, contactNumber.text, address.text):
        Fluttertoast.showToast(msg: 'all fields are required');
  } else {
    // Validation failed, show an error message or handle it accordingly
    Fluttertoast.showToast(msg: 'Please correct the errors in the form');
    }
    },




                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                        Colors
                            .transparent, // Set the background color to transparent
                      ),
                      overlayColor: MaterialStateProperty.all<Color>(
                        Colors.purple.shade600.withOpacity(
                            0.5), // Change the overlay color when pressed
                      ),
                    ),
                    child: Container(
                      height: 53,
                      width: double.infinity,
                      margin: const EdgeInsets.symmetric(horizontal: 30),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 2,
                            color: Colors.black12.withOpacity(1),
                            offset: const Offset(2, 2),
                          )
                        ],
                        borderRadius: BorderRadius.circular(100).copyWith(
                          bottomRight: const Radius.circular(0),
                        ),
                        gradient: LinearGradient(
                          colors: [
                            Colors.purple.shade600,
                            Colors.amber.shade900,
                          ],
                        ),
                      ),
                      child: Text(
                        'Register',
                        style: TextStyle(
                          color: Colors.white.withOpacity(.8),
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  const Text('Already have an account?',
                      style: TextStyle(color: Colors.white70, fontSize: 13)),
                  const SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const LoginPage()),
                      );
                    },
                    child: Container(
                      height: 53,
                      width: double.infinity,
                      margin: const EdgeInsets.symmetric(horizontal: 30),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 2,
                                color: Colors.black12.withOpacity(1),
                                offset: const Offset(2, 2))
                          ],
                          borderRadius: BorderRadius.circular(100),
                          gradient: LinearGradient(colors: [
                            Colors.orangeAccent.shade700,
                            Colors.greenAccent.shade400
                          ])),
                      child: Text('Login',
                          style: TextStyle(
                              color: Colors.white.withOpacity(.8),
                              fontSize: 15,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}