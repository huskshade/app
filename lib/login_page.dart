import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter/material.dart';
import 'package:savour/admin_side/home_page.dart';
import 'package:savour/admin_side/rest/rest_api.dart';
import 'admin_side/widgets/navBar.dart';
import 'constant/utils.dart';
import 'register_page.dart';
// import 'package:http/http.dart' as http;
// import 'package:fluttertoast/fluttertoast.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}



// Future login() async {
//   var url = Uri.http("192.168.0.104", '/login/login.php', {'q': '{http}'});
//   var response = await http.post(url, body: {
//     "username": user.text,
//     "password": pass.text,
//   });
//   var data = json.decode(response.body);
//   if (data.toString() == "Success") {
//     Fluttertoast.showToast(
//       msg: 'Login Successful',
//       backgroundColor: Colors.green,
//       textColor: Colors.white,
//       toastLength: Toast.LENGTH_SHORT,
//     );
//     Navigator.push(
//       context,
//       MaterialPageRoute(
//         builder: (context) => DashBoard(),
//       ),
//     );
//   } else {
//     Fluttertoast.showToast(
//       backgroundColor: Colors.red,
//       textColor: Colors.white,
//       msg: 'Username and password invalid',
//       toastLength: Toast.LENGTH_SHORT,
//     );
//   }
// }
TextEditingController username = TextEditingController();
TextEditingController pass = TextEditingController();
bool isPasswordVisible = false;

class _LoginPageState extends State<LoginPage> {
  final formKey = GlobalKey<FormState>();

  void _showSuccessSnackBar(BuildContext context) {
    const snackBar = SnackBar(content: Text("Success"));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.bottomRight,
                  end: Alignment.topRight,
                  colors: [
                Color.fromARGB(255, 80, 198, 241),
                Color.fromARGB(255, 223, 141, 58)
              ])),
          child: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  // container for logo
                  const CircleAvatar(
                    backgroundImage: AssetImage('lib/logo/logo.png'),
                    radius: 90,
                  ),
                  const SizedBox(
                    height: 40,
                  ),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child: TextFormField(
                      // text email controller here!
                      validator: (value) {
                        if (value!.isEmpty ||
                            !RegExp(r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$')
                                .hasMatch(value)) {
                          return "Enter Correct username!";
                        } else {
                          return null;
                        }
                      },
                      controller: username,
                      style:
                          const TextStyle(color: Colors.white, fontSize: 14.5),
                      decoration: InputDecoration(
                          prefixIconConstraints:
                              const BoxConstraints(minWidth: 45),
                          prefixIcon: const Icon(
                            Icons.alternate_email_outlined,
                            color: Colors.white70,
                            size: 22,
                          ),
                          //border: InputBorder.none,
                          hintText: 'Enter Username',
                          hintStyle: const TextStyle(
                              color: Colors.white60, fontSize: 14.5),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(90).copyWith(
                                  bottomRight: const Radius.circular(0)),
                              borderSide:
                                  const BorderSide(color: Colors.white38)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100).copyWith(
                                  bottomRight: const Radius.circular(0)),
                              borderSide:
                                  const BorderSide(color: Colors.white70))),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child: TextFormField(
                      validator: (value) {
                        if (value!.isEmpty ||
                            !RegExp(r'^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[\W_]).{8,}$')
                                .hasMatch(value)) {
                          return "Enter correct password!";
                        } else {
                          return null;
                        }
                      },
                      // password controller here!
                      controller: pass,
                      style:
                          const TextStyle(color: Colors.white, fontSize: 14.5),
                      obscureText: isPasswordVisible ? false : true,
                      decoration: InputDecoration(
                          prefixIconConstraints:
                              const BoxConstraints(minWidth: 45),
                          prefixIcon: const Icon(
                            Icons.lock,
                            color: Colors.white70,
                            size: 22,
                          ),
                          suffixIconConstraints:
                              const BoxConstraints(minWidth: 45, maxWidth: 46),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                isPasswordVisible = !isPasswordVisible;
                              });
                            },
                            child: Icon(
                              isPasswordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.white70.withOpacity(0.5),
                              size: 22,
                            ),
                          ),
                          // border: InputBorder.none,
                          hintText: 'Enter Password',
                          hintStyle: const TextStyle(
                              color: Colors.white60, fontSize: 14.5),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100).copyWith(
                                  bottomRight: const Radius.circular(0)),
                              borderSide:
                                  const BorderSide(color: Colors.white38)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100).copyWith(
                                  bottomRight: const Radius.circular(0)),
                              borderSide:
                                  const BorderSide(color: Colors.white70))),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),

                  // Login button
                  GestureDetector(
                    onTap: () {

                      username.text.isNotEmpty && pass.text.isNotEmpty ?
                      userLogin(context, username.text, pass.text) :
                          Fluttertoast.showToast(msg: 'all fields are required');
                      username.clear();
                      pass.clear();
                      // if (formKey.currentState!.validate()) {
                      //   //check if form data are valid
                      //   _showSuccessSnackBar(context);
                      //   Navigator.push(
                      //       context,
                      //       MaterialPageRoute(
                      //           builder: (context) => const NavBar()));
                      // }
                    },
                    child: Container(
                      height: 53,
                      width: double.infinity,
                      margin: const EdgeInsets.symmetric(horizontal: 30),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 2,
                                color: Colors.black12.withOpacity(1),
                                offset: const Offset(2, 2))
                          ],
                          borderRadius: BorderRadius.circular(100),
                        color: Colors.orange
                         ),
                      child: Text('Login',
                          style: TextStyle(
                              color: Colors.white.withOpacity(.8),
                              fontSize: 15,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  const Text('Don\'t have an account?',
                      style: TextStyle(color: Colors.white70, fontSize: 13)),
                  const SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {

                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (context) =>Register()),
                          (route) => route.isFirst);
                    },
                    child: Container(
                      height: 53,
                      width: double.infinity,
                      margin: const EdgeInsets.symmetric(horizontal: 30),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 2,
                              color: Colors.black12.withOpacity(1),
                              offset: const Offset(2, 2))
                        ],
                        borderRadius: BorderRadius.circular(100),
                        color: Colors.green,
                        border: Border.all(color: Colors.white60),
                      ),
                      child: Text('Sign Up',
                          style: TextStyle(
                              color: Colors.white.withOpacity(.8),
                              fontSize: 15,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  // doLogin(String username, String pass)async{
  //   var res = await userLogin(username.trim(), pass.trim());
  //             return res;
  //
  // }


}

